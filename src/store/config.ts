import { reactive, toRefs } from "vue";

const config = reactive({
    memes: "kwejk",
    loaded: false,
});

export default function useConfig() {
    const setConfig = function (memes: string) {
        config.memes = memes;
    };

    return {
        ...toRefs(config),
        setConfig,
    };
}
